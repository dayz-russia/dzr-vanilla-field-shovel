class CfgPatches
{
	class dzr_vanilla_field_shovel
	{
		requiredAddons[] = {"DZ_Data", "DZ_Scripts", "DZ_Weapons_Melee", "DZ_Gear_Tools"};
		units[] = {};
		weapons[] = {};
	};
};
class CfgMods
{
	class dzr_vanilla_field_shovel
	{
		type = "mod";
		author = "DayZ Russia";
		dir = "dzr_vanilla_field_shovel";
		name = "DZR Vanilla Field Shovel";
		dependencies[] = {"World"};
		class defs
		{
			class worldScriptModule
			{
				files[] = {"dzr_vanilla_field_shovel/4_World"};
			};
		};
	};
};
class CfgVehicles
{
	class Inventory_Base;
	
	class FieldShovel: Inventory_Base
	{
		displayName = "$STR_DZR_FieldShovel0";
		descriptionShort = "$STR_DZR_FieldShovel1";
		itemSize[] = {2,3};
		weight = 1000;
	};
	
	class Shovel;
	class dzr_FieldShovel: Shovel
	{
		scope = 2;
		displayName = "$STR_DZR_FieldShovel0";
		descriptionShort = "$STR_DZR_FieldShovel1";
		model = "\dz\gear\tools\CSLA_Field_Shovel.p3d";
		rotationFlags = 17;
		suicideAnim = "woodaxe";
		itemSize[] = {2,3};
		weight = 1100;
		fragility = 0.008;
		itemBehaviour = 2;
		inventorySlot[] = {"Shoulder","Melee"};		
		build_action_type = 4;
		dismantle_action_type = 4;
		openItemSpillRange[] = {10,40};
		class DamageSystem
		{
			class GlobalHealth
			{
				class Health
				{
					hitpoints = 250;
					healthLevels[] = {{1.0,{"DZ\gear\tools\data\CSLA_Field_Shovel.rvmat"}},{0.7,{"DZ\gear\tools\data\CSLA_Field_Shovel.rvmat"}},{0.5,{"DZ\gear\tools\data\CSLA_Field_Shovel_damage.rvmat"}},{0.3,{"DZ\gear\tools\data\CSLA_Field_Shovel_damage.rvmat"}},{0.0,{"DZ\gear\tools\data\CSLA_Field_Shovel_destruct.rvmat"}}};
				};
			};
		};
		class MeleeModes
		{
			class Default
			{
				ammo = "MeleeFieldShovel";
				range = 1.4;
			};
			class Heavy
			{
				ammo = "MeleeFieldShovel_Heavy";
				range = 1.4;
			};
			class Sprint
			{
				ammo = "MeleeFieldShovel_Heavy";
				range = 3.3;
			};
		};
		class Horticulture
		{
			ToolAnim = "DIGGINGSHOVEL";
			DiggingTimeToComplete = 6.0;
		};
		class AnimEvents
		{
			class SoundWeapon
			{
				class pickUpShovel_Light
				{
					soundSet = "pickUpShovelLight_SoundSet";
					id = 796;
				};
				class pickUpShovel
				{
					soundSet = "pickUpShovel_SoundSet";
					id = 797;
				};
				class drop
				{
					soundset = "shovel_drop_SoundSet";
					id = 898;
				};
				class ShoulderR_Hide
				{
					soundset = "ShoulderR_Hide_SoundSet";
					id = 1210;
				};
				class ShoulderR_Show
				{
					soundset = "ShoulderR_Show_SoundSet";
					id = 1211;
				};
			};
		};
	};
};