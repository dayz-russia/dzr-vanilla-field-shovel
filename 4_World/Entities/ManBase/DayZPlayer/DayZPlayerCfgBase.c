modded class PlayerBase extends ManBase
{
    override void Init()
    {
        if ( !GetGame().IsServer() || !GetGame().IsMultiplayer() ) {
            DayzPlayerItemBehaviorCfg     toolsOneHanded = new DayzPlayerItemBehaviorCfg;
            toolsOneHanded.SetToolsOneHanded();
            GetDayZPlayerType().AddItemInHandsProfileIK("dzr_FieldShovel", "dz/anims/workspaces/player/player_main/props/player_main_1h_handshovel.asi", toolsOneHanded,		"dz/anims/anm/player/ik/gear/CSLA_Field_Shovel.anm");
            }
        super.Init();
    }
}